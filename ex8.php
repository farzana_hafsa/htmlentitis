<?php
$input_string="PHP 'character string conversion' functions <i>htmlentities()</i>";
$output = htmlentities($input_string);
echo "<b>Original Character String</b><br/>";
echo $input_string."<br/><br/>";
echo "<b>After Conversion</b><br/>";
echo $output;
?>
